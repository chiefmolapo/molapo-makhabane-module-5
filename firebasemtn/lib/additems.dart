import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import './datalist.dart';

class AddItems extends StatefulWidget {
  const AddItems({Key? key}) : super(key: key);

  @override
  State<AddItems> createState() => _AddItemsState();
}

class _AddItemsState extends State<AddItems> {
  TextEditingController laptopcontroller = TextEditingController();
  TextEditingController chaircontroller = TextEditingController();
  TextEditingController namecontroller = TextEditingController();

  Future _addITemsData() {
    final laptop = laptopcontroller.text;
    final chairname = chaircontroller.text;
    final name = namecontroller.text;

    final refVar = FirebaseFirestore.instance.collection('ADD ITEMS').doc();
    return refVar
        .set({"laptopmodel": laptop, "chairname": chairname, "your_name": name})
        .then((value) => log("send Success"))
        .catchError((onError) => log(onError));
  }

  @override
  Widget build(BuildContext context) {
    return
        // Column(
        // children: [
        MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: const Text('ADD PRODUCTS'),
            backgroundColor: Color.fromARGB(248, 240, 206, 15),
            foregroundColor: Colors.black),
        body: Padding(
          padding: const EdgeInsets.all(30),
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            const Text(
              'Hello ADD UR PRODUCT',
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.w600),
            ),
            TextField(
              controller: laptopcontroller,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Laptop Name',
                  prefixIcon: Icon(Icons.laptop)),
            ),
            const SizedBox(height: 20),
            TextField(
              controller: chaircontroller,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'chairname',
                  prefixIcon: Icon(Icons.chair)),
            ),
            const SizedBox(height: 20),
            TextField(
              controller: namecontroller,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'enter your name',
                  prefixIcon: Icon(Icons.person_off)),
            ),
            const SizedBox(height: 20),
            Container(
                height: 40,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.yellow,
                    borderRadius: BorderRadius.circular(100),
                    gradient: const LinearGradient(
                        colors: [Colors.amber, Colors.orangeAccent])),
                child: MaterialButton(
                  onPressed: () {
                    _addITemsData();
                  },
                  child: const Text(
                    'ADD PRODUCTS',
                    style: TextStyle(
                      color: Color.fromARGB(239, 58, 52, 52),
                      fontSize: 25,
                    ),
                  ),
                )),
            const SizedBox(
              height: 40,
            ),
            Row(
              children: [DataListItems()],
            )
          ]),
        ),
      ),

      //
    );
    //       DataListItems()
    //     ],
    //   );
  }
}
