import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DataListItems extends StatefulWidget {
  const DataListItems({Key? key}) : super(key: key);

  @override
  State<DataListItems> createState() => _DataListItemsState();
}

class _DataListItemsState extends State<DataListItems> {
  //ffjfjfjffj
  final Stream<QuerySnapshot> myDataList =
      FirebaseFirestore.instance.collection('ADD ITEMS').snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: myDataList,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }

        return Expanded(
          child: ListView(
            shrinkWrap: true,
            children: snapshot.data!.docs
                .map((DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                  return ListTile(
                      isThreeLine: true,
                      title: Text(data['chairname']),
                      subtitle:Row (mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(data['laptopmodel']),
                          Text(data['your_name']),
                        ],
                      )
                      // Text(data['your_name']),
                      );
                })
                .toList()
                .cast(),
          ),
        );
      },
    );
  }
}



//  return StreamBuilder(
//       stream: myDataList,
//       builder: (BuildContext context,
//           AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
//         if (snapshot.hasError) {
//           return Text("There is an error");
//         }
//         if (snapshot.connectionState == ConnectionState.waiting) {
//           return CircularProgressIndicator();
//         }
//         if (snapshot.hasData) {
//           return Row(
//             children: [
//               Expanded(
//                 child: SizedBox(
//                   height: MediaQuery.of(context).size.height,
//                   width: MediaQuery.of(context).size.width,
//                   child: ListView(
//                       shrinkWrap: true,
//                       children: snapshot.data!.docs
//                           .map((DocumentSnapshot documentSnapshot) {
//                             Map<String, dynamic> data = documentSnapshot.data()!
//                                 as Map<String, dynamic>;
//                             return ListTile(
//                                 isThreeLine: true,
//                                 // leading: Text(data['chairname']),
//                                 title: Text(data['chairname']),
//                                 subtitle: Column(
//                                   children: [
//                                     Text(data['laptopmodel']),
//                                     Text(data['your_name']),
//                                   ],
//                                 ));
//                           })
//                           .toList()
//                           .cast()),
//                 ),
//               )
//             ],
//           );
//         } else {
//           return Center(
//             child: CircularProgressIndicator(),
//           );
//         }
//       },
//     );
//   }