import 'package:firebase_core/firebase_core.dart';
import 'package:firebasemtn/additems.dart';
import 'package:flutter/material.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyCiJ0hMWtHleXzWSrHqEeATqZmfXHxeU78",
          authDomain: "fir-mtnapp.firebaseapp.com",
          projectId: "fir-mtnapp",
          storageBucket: "fir-mtnapp.appspot.com",
          messagingSenderId: "151399392599",
          appId: "1:151399392599:web:c1257bd29341e8fae6ca12"));

  //     //options: DefaultFirebaseOptions.currentPlatform
  //     );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(title: ''),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return AddItems();
  }
}
